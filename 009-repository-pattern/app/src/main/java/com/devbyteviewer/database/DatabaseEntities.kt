/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devbyteviewer.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.devbyteviewer.domain.DevByteVideo


/**
 * Database entities go in this file. These are responsible for reading and writing from the
 * database.
 */


/**
 * DatabaseVideo represents a video entity in the database.
 */
@Entity(tableName = "databasevideo")
data class DatabaseVideo (
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id")       val id: Int = 0,
    @ColumnInfo(name = "url")                                       val url: String,
    @ColumnInfo(name = "updated")                                   val updated: String,
    @ColumnInfo(name = "title")                                     val title: String,
    @ColumnInfo(name = "description")                               val description: String,
    @ColumnInfo(name = "thumbnail")                                 val thumbnail: String
)


/**
 * Map DatabaseVideo to domain entity
 */
fun DatabaseVideo.asDomainModel(): DevByteVideo {
    return DevByteVideo(
        id = id,
        url = url,
        title = title,
        description = description,
        updated = updated,
        thumbnail = thumbnail
    )
}
