# android-basics-kotlin
Contains code from the [[Android Basics in Kotlin](https://developer.android.com/courses/android-basics-kotlin/course)] course series. \
This is useful to show people some code plus you can check [[my completion badges](https://g.dev/jeremy-rouanet)] :-) \
You will find below the completed labs and the associated learnings.

# Labs
##  001-build-a-basic-layout-with-compose
[[Lab Link](https://developer.android.com/courses/pathways/android-basics-kotlin-three)] \
Learnings : Theming (light mode only), Composables (Text, Image, Column, Rows), Preview.

## 002-interactive-dice-roller-with-compose
[[Lab Link](https://developer.android.com/courses/pathways/android-basics-kotlin-four)] \
Learnings : Theming (light mode / dark mode toggle), UI Event (onClick), Recomposition (remember, MutableState).

## 003-lemonade-with-compose
[[Lab Link](https://developer.android.com/codelabs/basic-android-kotlin-training-project-lemonade)] \
Learnings : Navigation, Screens and Preview, Instrumented tests (semantics).

## 004-tip-calculator
[[Lab Link](https://developer.android.com/codelabs/basic-android-kotlin-training-tip-calculator)] \
Learnings : ViewModel, Data binding (bi-directional), XML layout, InverseMethod, data class, companion object.

## 005-affirmations
[[Lab Link](https://developer.android.com/codelabs/basic-android-kotlin-training-recyclerview-scrollable-list)] \
Learnings : RecyclerView, ListAdapter, DiffUtil, ViewHolder, Espresso.

## 006-marsphotos
[[Lab Link](https://developer.android.com/codelabs/basic-android-kotlin-training-internet-images)] \
Learnings : Infinite List, Retrofit - Moshi - Coil, Binding Adapters, Coroutines, LiveData.

## 007-bus-scheduler
[[Lab Link](https://developer.android.com/codelabs/basic-android-kotlin-training-intro-room-flow)] \
Learnings : Room, Flow.

## 008-inventory
[[Lab Link - part 1](https://developer.android.com/codelabs/basic-android-kotlin-training-persisting-data-room)] \
[[Lab Link - part 2](https://developer.android.com/codelabs/basic-android-kotlin-training-update-data-room)] \
Learnings : Room, Flow, Navigation, Data binding (InverseMethod, Bindable), Form Validation.

## 009-repository-pattern
[[Lab Link](https://developer.android.com/codelabs/basic-android-kotlin-training-repository-pattern)] \
Learnings : Infinite List with Paging3, Repository pattern (fetch data from network or local database depending on network availability), Retrofit - Moshi - Glide, Room, Flow.

## 010-words
[[Lab Link](https://developer.android.com/codelabs/basic-android-kotlin-training-preferences-datastore)] \
Learnings : Jetpack DataStore (Preferences).

## 011-workmanager
[[Lab Link](https://developer.android.com/codelabs/android-workmanager)] \
Learnings : WorkManager (Chaining, Constraints, Unique, Cancellation, WorkInfo), Notifications.