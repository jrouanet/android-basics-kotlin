package com.lemonadewithcompose.ui

import androidx.compose.foundation.clickable
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.lemonadewithcompose.Destination
import com.lemonadewithcompose.R
import com.lemonadewithcompose.ui.theme.LemonadeWithComposeTheme


@Composable
fun Screen3(navController: NavHostController) {

    fun clickImage() {
        navController.navigate(Destination.startAgain.route)
    }

    Root {
        //Set Screen content :
        TextHolder(pTextRscId = R.string.click_to_drink_your_lemonade)
        ImageHolder(
            R.drawable.lemon_drink,
            R.string.click_to_drink_your_lemonade_image_description
        ) {
            Modifier.clickable { clickImage() }
        }
    }
}

@Preview
@Composable
fun PreviewScreen3() {
    LemonadeWithComposeTheme() {
        Screen3(rememberNavController())
    }
}

