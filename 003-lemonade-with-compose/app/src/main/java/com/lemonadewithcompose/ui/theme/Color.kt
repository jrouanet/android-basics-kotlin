package com.lemonadewithcompose.ui.theme

import androidx.compose.ui.graphics.Color

val yellow_light: Color = Color(0xffffff8b)
val yellow_primary: Color = Color(0xffffee58)
val yellow_dark: Color = Color(0xffFFD600)
val teal_200: Color = Color(0xFF03DAC5)
val teal_700: Color = Color(0xFF018786)
val black: Color = Color(0xFF000000)
val white: Color = Color(0xFFFFFFFF)