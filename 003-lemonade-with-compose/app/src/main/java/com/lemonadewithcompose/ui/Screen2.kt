package com.lemonadewithcompose.ui

import androidx.compose.foundation.clickable
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.lemonadewithcompose.Destination
import com.lemonadewithcompose.R
import com.lemonadewithcompose.ui.theme.LemonadeWithComposeTheme


@Composable
fun Screen2(navController: NavHostController) {
    var lemonPressedCount: Int = 0
    val randomMax: Int = 3
    val lemonPressedCountNeeded: Int = (1..randomMax).random()

    fun clickImage(): Unit {
        lemonPressedCount++
        if (lemonPressedCount == lemonPressedCountNeeded) {
            navController.navigate(Destination.drinkYourLemonade.route)
        }
    }

    Root {
        //Set Screen content :
        TextHolder(pTextRscId = R.string.click_to_juice_the_lemon)
        ImageHolder(R.drawable.lemon_squeeze, R.string.click_to_juice_the_lemon_image_description) {
            Modifier.clickable { clickImage() }
        }
    }
}

@Preview
@Composable
fun PreviewScreen2() {
    LemonadeWithComposeTheme() {
        Screen2(rememberNavController())
    }
}


