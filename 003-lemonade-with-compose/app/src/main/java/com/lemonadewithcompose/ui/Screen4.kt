package com.lemonadewithcompose.ui

import androidx.compose.foundation.clickable
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.lemonadewithcompose.Destination
import com.lemonadewithcompose.R
import com.lemonadewithcompose.ui.theme.LemonadeWithComposeTheme


@Composable
fun Screen4(navController: NavHostController) {

    fun clickImage() {
        navController.navigate(Destination.selectALemon.route) {
            popUpTo(Destination.selectALemon.route) {
                inclusive = true
            }
        }
    }

    Root {
        //Set Screen content :
        TextHolder(pTextRscId = R.string.click_to_start_again)
        ImageHolder(R.drawable.lemon_restart, R.string.click_to_start_again_image_description) {
            Modifier.clickable { clickImage() }
        }
    }
}

@Preview
@Composable
fun PreviewScreen4() {
    LemonadeWithComposeTheme() {
        Screen4(rememberNavController())
    }
}



