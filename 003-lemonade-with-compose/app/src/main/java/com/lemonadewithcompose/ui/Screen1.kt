package com.lemonadewithcompose.ui

import androidx.compose.foundation.clickable
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.lemonadewithcompose.Destination
import com.lemonadewithcompose.R
import com.lemonadewithcompose.ui.theme.LemonadeWithComposeTheme


@Composable
fun Screen1(navController: NavHostController) {

    fun clickImage(): Unit {
        navController.navigate(Destination.pressALemon.route)
    }

    Root {
        //Set Screen content :
        TextHolder(pTextRscId = R.string.click_to_select_a_lemon)
        ImageHolder(R.drawable.lemon_tree, R.string.click_to_select_a_lemon_image_description) {
            Modifier.clickable { clickImage() }
        }
    }
}

@Preview
@Composable
fun PreviewScreen1() {
    LemonadeWithComposeTheme() {
        Screen1(rememberNavController())
    }
}