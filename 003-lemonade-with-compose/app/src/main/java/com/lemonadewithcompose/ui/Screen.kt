package com.lemonadewithcompose.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.lemonadewithcompose.R
import com.lemonadewithcompose.ui.theme.LemonadeWithComposeTheme

/**
 * Contains the root layout of the screens.
 * The content is defined in the trailing lambda.
 */
@Composable
fun Root(pContent: @Composable () -> Unit) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(stringResource(id = R.string.app_name)) },
                backgroundColor = MaterialTheme.colors.primary,
                contentColor = MaterialTheme.colors.onBackground
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .padding(paddingValues = it),
            verticalArrangement = Arrangement.Center
        ) {
            pContent()
        }
    }
}

@Preview
@Composable
fun PreviewRoot() {
    LemonadeWithComposeTheme() {
        Root() {

        }
    }
}

/**
 * Defines the text in the middle.
 */
@Composable
fun TextHolder(pTextRscId: Int) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            text = stringResource(id = pTextRscId),
            fontSize = 18.sp,
            modifier = Modifier.testTag("textHolder")
        )
    }
}

/**
 * Defines the drawable the user can click on to move the application state.
 */
@Composable
fun ImageHolder(drawableRscId: Int, contentDescriptionRscId: Int, modifier: () -> Modifier) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Image(
            painter = painterResource(id = drawableRscId),
            contentDescription = stringResource(id = contentDescriptionRscId),
            modifier = modifier().testTag("imageHolder")
        )
    }
}