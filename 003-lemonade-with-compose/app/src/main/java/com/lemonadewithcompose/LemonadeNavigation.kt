package com.lemonadewithcompose

import androidx.compose.runtime.Composable
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.lemonadewithcompose.ui.Screen1
import com.lemonadewithcompose.ui.Screen2
import com.lemonadewithcompose.ui.Screen3
import com.lemonadewithcompose.ui.Screen4

/**
 * Defines all the navigation graphs.
 */
internal sealed class Graph(val route: String) {
    object LemonadeGraph1 : Graph("lemonadeGraph1")
}

/**
 * Defines all the destinations.
 */
internal sealed class Destination(val route: String) {
    object selectALemon : Destination("screen1")
    object pressALemon : Destination("screen2")
    object drinkYourLemonade : Destination("screen3")
    object startAgain : Destination("screen4")
}

/**
 * Adds graphs to the navigation component.
 */
@Composable
internal fun LemonadeNavigation(navController: NavHostController) {
    NavHost(navController = navController, startDestination = Graph.LemonadeGraph1.route) {
        addLemonadeGraph1(navController)
    }
}

/**
 * Links destinations to graph.
 */
private fun NavGraphBuilder.addLemonadeGraph1(navController: NavHostController) {
    navigation(
        route = Graph.LemonadeGraph1.route,
        startDestination = Destination.selectALemon.route
    ) {
        addSelectALemonDestination(navController)
        addPressALemonDestination(navController)
        addDrinkYourLemonadeDestination(navController)
        addStartAgainDestination(navController)
    }
}

/**
 * Functions below link each composable to its route.
 */
private fun NavGraphBuilder.addSelectALemonDestination(navController: NavHostController) {
    composable(route = Destination.selectALemon.route) { Screen1(navController) }
}

private fun NavGraphBuilder.addPressALemonDestination(navController: NavHostController) {
    composable(route = Destination.pressALemon.route) { Screen2(navController) }
}

private fun NavGraphBuilder.addDrinkYourLemonadeDestination(navController: NavHostController) {
    composable(route = Destination.drinkYourLemonade.route) { Screen3(navController) }
}

private fun NavGraphBuilder.addStartAgainDestination(navController: NavHostController) {
    composable(route = Destination.startAgain.route) { Screen4(navController) }
}



