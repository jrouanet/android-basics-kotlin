package com.lemonadewithcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.lemonadewithcompose.ui.theme.LemonadeWithComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LemonadeWithComposeTheme {
                PageContent()
            }
        }
    }
}

@Composable
fun PageContent() {
    //Navigate between pages.
    val navController: NavHostController = rememberNavController()
    LemonadeNavigation(navController = navController)
}