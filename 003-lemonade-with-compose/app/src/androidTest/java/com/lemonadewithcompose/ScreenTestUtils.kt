package com.lemonadewithcompose

import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.ComposeTestRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick

/**
 * Contains common methods to test our composables.
 */
class ScreenTestUtils {
    companion object {
        val textHolderTag: String = "textHolder"
        val imageHolderTag: String = "imageHolder"

        /**
         * Click on the image in the center of the screen to move states.
         */
        fun clickOnImage(composeTestRule: ComposeTestRule): Unit {
            composeTestRule.onNodeWithTag(imageHolderTag)
                .performClick()
        }

        /**
         * Check that the correct composable is displayed.
         * @TODO : Test that the drawable is correct with semantics.
         */
        fun testState(
            composeTestRule: ComposeTestRule,
            textHolderString: String
        ) {
            composeTestRule.onNodeWithTag(textHolderTag, useUnmergedTree = true)
                .assertTextEquals(textHolderString)
        }
    }
}