package com.lemonadewithcompose

import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.lemonadewithcompose.ui.theme.LemonadeWithComposeTheme
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class Screen2IsolationTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    lateinit var navController: NavHostController
    lateinit var textHolderExpected: String
    lateinit var nextScreenTextHolderExpected: String

    @Before
    fun setUp() {
        composeTestRule.setContent {
            textHolderExpected = stringResource(id = R.string.click_to_juice_the_lemon)
            nextScreenTextHolderExpected =
                stringResource(id = R.string.click_to_drink_your_lemonade)
            navController = rememberNavController()

            LemonadeWithComposeTheme {
                LemonadeNavigation(navController = navController)
                navController.navigate(Destination.pressALemon.route)
            }
        }
    }

    /**
     * Test the view components of the pick lemon state
     */
    @Test
    fun `test_initial_state`() {
        ScreenTestUtils.testState(composeTestRule, textHolderExpected)
    }

    /**
     * Test that the squeeze functionality takes us to the "drink state"
     */
    @Test
    fun `test_squeezing_lemon_proceeds_to_drink_state`() {
        Screen2IsolationTestUtils.juiceLemon(composeTestRule, textHolderExpected)
        ScreenTestUtils.testState(composeTestRule, nextScreenTextHolderExpected)
    }

}