package com.lemonadewithcompose

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.ComposeTestRule
import androidx.compose.ui.test.onNodeWithText

/**
 * Contains methods to interact with the component.
 */
class Screen2IsolationTestUtils {

    companion object {
        /**
         * Squeeze the lemon until the lemon image is gone.
         * The number of clicks required is determined by a random number that the test is not
         * aware of, so we loop and click until the image changes.
         */
        fun juiceLemon(
            composeTestRule: ComposeTestRule,
            textHolder: String
        ) {
            var failsafe: Int = 10

            while (true) {
                try {
                    composeTestRule.onNodeWithText(textHolder).assertIsDisplayed()
                    // Click image to squeeze the lemon
                    ScreenTestUtils.clickOnImage(composeTestRule)
                    if (failsafe-- == 0) break //To avoid infinite loop if the state doesn't change at all.
                } catch (e: AssertionError) {
                    //If the state has changed, break loop.
                    break
                }
            }
        }
    }
}