package com.lemonadewithcompose


import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class LemonadeWithComposeIntegrationTest {
    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    /**
     * Test the view components of the pick lemon state
     */
    @Test
    fun `test_initial_state`() {
        ScreenTestUtils.testState(
            composeTestRule,
            composeTestRule.activity.getString(R.string.click_to_select_a_lemon)
        )
    }

    /**
     * Test that the pick lemon functionality takes us to the "squeeze state"
     */
    @Test
    fun `test_picking_lemon_proceeds_to_squeeze_state`() {
        // Click image to progress state
        ScreenTestUtils.clickOnImage(composeTestRule)
        ScreenTestUtils.testState(
            composeTestRule,
            composeTestRule.activity.getString(R.string.click_to_juice_the_lemon)
        )
    }

    /**
     * Test that the squeeze functionality takes us to the "drink state"
     */
    @Test
    fun `test_squeezing_lemon_proceeds_to_drink_state`() {
        test_picking_lemon_proceeds_to_squeeze_state()
        Screen2IsolationTestUtils.juiceLemon(
            composeTestRule,
            composeTestRule.activity.getString(R.string.click_to_juice_the_lemon)
        )
        ScreenTestUtils.testState(
            composeTestRule,
            composeTestRule.activity.getString(R.string.click_to_drink_your_lemonade)
        )
    }

    /**
     * Test that the drink functionality takes us to the "restart state"
     */
    @Test
    fun `test_drinking_juice_proceeds_to_restart_state`() {
        test_squeezing_lemon_proceeds_to_drink_state()
        ScreenTestUtils.clickOnImage(composeTestRule)
        ScreenTestUtils.testState(
            composeTestRule,
            composeTestRule.activity.getString(R.string.click_to_start_again)
        )
    }

    /**
     * Test that the restart functionality takes us back to the "pick lemon state"
     */
    @Test
    fun `test_restarting_proceeds_to_pick_lemon_state`() {
        test_drinking_juice_proceeds_to_restart_state()
        ScreenTestUtils.clickOnImage(composeTestRule)
        ScreenTestUtils.testState(
            composeTestRule,
            composeTestRule.activity.getString(R.string.click_to_select_a_lemon)
        )
    }
}