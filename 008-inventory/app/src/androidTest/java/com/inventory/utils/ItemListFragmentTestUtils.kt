package com.inventory

import android.view.View
import androidx.test.espresso.Espresso
import androidx.test.espresso.PerformException
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import com.inventory.adapters.ItemListAdapter
import com.inventory.data.Item
import org.hamcrest.Matcher
import org.hamcrest.Matchers

class ItemListFragmentTestUtils {
    companion object {
        fun itemMatcher(item: Item): Matcher<View> {
            return ViewMatchers.hasDescendant(
                Matchers.allOf(
                    ViewMatchers.withText(item.itemName),
                    ViewMatchers.hasSibling(ViewMatchers.withText(Matchers.containsString(item.itemPrice.toString()))),
                    ViewMatchers.hasSibling(
                        ViewMatchers.withText(Matchers.containsString(item.quantityInStock.toString()))
                    )
                )
            )
        }

        fun scrollToItem(item: Item): ViewInteraction {
            return Espresso.onView(ViewMatchers.withId(R.id.recyclerView)).perform(
                RecyclerViewActions.scrollTo<ItemListAdapter.ItemViewHolder>(
                    itemMatcher(item)
                )
            )
        }

        fun checkIfItemInList(item: Item) {
            scrollToItem(item).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        }

        fun checkIfItemNotInList(item: Item) {
            try {
                checkIfItemInList(item)
                check(false)
            } catch (e: PerformException) {
                check(true)
            }
        }

        fun clickOnItem(item: Item) {
            scrollToItem(item).perform(ViewActions.click())
        }
    }
}
