package com.inventory.utils

import android.content.Context
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.inventory.R
import com.inventory.data.Item
import org.hamcrest.Matchers

class ItemDetailFragmentTestUtils {
    companion object {
        fun checkFieldsEqual(item: Item) {
            Espresso.onView(ViewMatchers.withId(R.id.item_name)).check(
                ViewAssertions.matches(
                    ViewMatchers.withText(
                        Matchers.containsString(item.itemName)
                    )
                )
            )
            Espresso.onView(ViewMatchers.withId(R.id.item_price)).check(
                ViewAssertions.matches(
                    ViewMatchers.withText(
                        Matchers.containsString(item.itemPrice.toString())
                    )
                )
            )
            Espresso.onView(ViewMatchers.withId(R.id.item_count)).check(
                ViewAssertions.matches(
                    ViewMatchers.withText(
                        Matchers.containsString(item.quantityInStock.toString())
                    )
                )
            )
        }

        fun sellItem() {
            Espresso.onView(ViewMatchers.withId(R.id.sell_item)).perform(ViewActions.click())
        }


        fun checkSellButtonStatus(enabled: Boolean) {
            if (enabled) {
                Espresso.onView(ViewMatchers.withId(R.id.sell_item)).check(
                    ViewAssertions.matches(
                        ViewMatchers.isEnabled()
                    )
                )
            } else {
                Espresso.onView(ViewMatchers.withId(R.id.sell_item)).check(
                    ViewAssertions.matches(
                        ViewMatchers.isNotEnabled()
                    )
                )
            }

        }

        fun deleteItem(context: Context) {
            Espresso.onView(ViewMatchers.withId(R.id.delete_item)).perform(ViewActions.click())
            TestUtils.clickOnDialogButton(context, R.string.yes)
        }

        fun editItem() {
            Espresso.onView(ViewMatchers.withId(R.id.edit_item)).perform(ViewActions.click())
        }
    }
}
