package com.inventory

import android.content.Context
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.inventory.data.Item

class AddItemFragmentTestUtils {
    companion object {
        fun typeInItemFields(item: Item) {
            Espresso.onView(ViewMatchers.withId(R.id.item_name))
                .perform(ViewActions.clearText())
                .perform(ViewActions.typeText(item.itemName))
            Espresso.onView(ViewMatchers.withId(R.id.item_price))
                .perform(ViewActions.clearText())
                .perform(ViewActions.typeText(item.itemPrice.toString()))
            Espresso.onView(ViewMatchers.withId(R.id.item_count))
                .perform(ViewActions.clearText())
                .perform(ViewActions.typeText(item.quantityInStock.toString()))
            Espresso.closeSoftKeyboard()
        }

        fun clearFields() {
            Espresso.onView(ViewMatchers.withId(R.id.item_name))
                .perform(ViewActions.clearText())
            Espresso.onView(ViewMatchers.withId(R.id.item_price))
                .perform(ViewActions.clearText())
            Espresso.onView(ViewMatchers.withId(R.id.item_count))
                .perform(ViewActions.clearText())
            Espresso.closeSoftKeyboard()
        }

        fun save() {
            Espresso.onView(ViewMatchers.withId(R.id.save_action)).perform(ViewActions.click())
        }

        fun checkIfErrorMessageIsDisplayed(context: Context, fieldId: Int, fieldNameStrRscId: Int) {
            val errorMsg = context.getString(
                R.string.error_cannot_be_empty_format,
                context.getString(fieldNameStrRscId)
            )
            Espresso.onView(ViewMatchers.withId(fieldId))
                .check(ViewAssertions.matches(ViewMatchers.hasErrorText(errorMsg)));
        }

        fun checkIfErrorMessagesAreDisplayed(context: Context) {
            checkIfErrorMessageIsDisplayed(context, R.id.item_name, R.string.item_name_req)
            checkIfErrorMessageIsDisplayed(context, R.id.item_price, R.string.item_price_req)
            checkIfErrorMessageIsDisplayed(context, R.id.item_count, R.string.quantity_req)
        }


    }
}
