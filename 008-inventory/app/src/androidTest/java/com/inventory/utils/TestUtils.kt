package com.inventory.utils

import android.content.Context
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers

class TestUtils {
    companion object {
        fun clickOnDialogButton(context: Context, strRscId: Int) {
            Espresso.onView(ViewMatchers.withText(context.getString(strRscId)))
                .inRoot(RootMatchers.isDialog())
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
                .perform(ViewActions.click());
        }
    }
}
