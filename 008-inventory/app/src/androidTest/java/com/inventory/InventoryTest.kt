package com.inventory

import android.content.Context
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.inventory.data.Item
import com.inventory.data.ItemRoomDatabase
import com.inventory.utils.ItemDetailFragmentTestUtils
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class InventoryTest {

    @get:Rule
    val activityScenarioRule = ActivityScenarioRule<MainActivity>(MainActivity::class.java)

    lateinit var db: ItemRoomDatabase
    lateinit var context: Context

    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().targetContext
        db = ItemRoomDatabase.getDatabase(context)

    }

    @Test
    fun items_in_db_should_be_listed() {
        runBlocking {
            for (i in 1..20) {
                db.itemDao().insert(
                    Item(
                        itemName = "item${i}",
                        itemPrice = i.toDouble(),
                        quantityInStock = i.toInt()
                    )
                )
            }
            ItemListFragmentTestUtils.checkIfItemInList(
                Item(
                    itemName = "item20",
                    itemPrice = 20.0,
                    quantityInStock = 20
                )
            )
        }
    }

    @Test
    fun create_item_test() {
        val item = Item(itemName = "item1", itemPrice = 1.0, quantityInStock = 2)
        onView(withId(R.id.floatingActionButton)).perform(ViewActions.click())
        AddItemFragmentTestUtils.typeInItemFields(item)
        AddItemFragmentTestUtils.save()
        ItemListFragmentTestUtils.checkIfItemInList(item)
    }


    @Test
    fun create_item_validation_test() {
        onView(withId(R.id.floatingActionButton)).perform(ViewActions.click())
        AddItemFragmentTestUtils.clearFields()
        AddItemFragmentTestUtils.save()
        AddItemFragmentTestUtils.checkIfErrorMessagesAreDisplayed(context)
    }

    @Test
    fun sell_item_test() {
        runBlocking {
            val item = Item(itemName = "item1", itemPrice = 1.0, quantityInStock = 1)
            db.itemDao().insert(item)
            ItemListFragmentTestUtils.clickOnItem(item)
            ItemDetailFragmentTestUtils.checkFieldsEqual(item)
            ItemDetailFragmentTestUtils.sellItem()
            ItemDetailFragmentTestUtils.checkFieldsEqual(item.copy(quantityInStock = item.quantityInStock - 1))
        }
    }

    @Test
    fun sell_item_if_out_of_stock_test() {
        runBlocking {
            val item = Item(itemName = "item1", itemPrice = 1.0, quantityInStock = 0)
            db.itemDao().insert(item)
            ItemListFragmentTestUtils.clickOnItem(item)
            ItemDetailFragmentTestUtils.checkSellButtonStatus(false)
        }
    }

    @Test
    fun sell_item_sell_button_toggle_test() {
        runBlocking {
            val item = Item(itemName = "item1", itemPrice = 1.0, quantityInStock = 1)
            db.itemDao().insert(item)
            ItemListFragmentTestUtils.clickOnItem(item)
            ItemDetailFragmentTestUtils.checkSellButtonStatus(true)
            ItemDetailFragmentTestUtils.sellItem()
            ItemDetailFragmentTestUtils.checkSellButtonStatus(false)
        }
    }

    @Test
    fun delete_item_test() {
        runBlocking {
            val item = Item(itemName = "item1", itemPrice = 1.0, quantityInStock = 1)
            db.itemDao().insert(item)
            ItemListFragmentTestUtils.clickOnItem(item)
            ItemDetailFragmentTestUtils.deleteItem(context)
            ItemListFragmentTestUtils.checkIfItemNotInList(item)
        }
    }

    @Test
    fun edit_item_test() {
        runBlocking {
            val item1 = Item(itemName = "item1", itemPrice = 1.0, quantityInStock = 1)
            val item2 = Item(itemName = "item2", itemPrice = 2.0, quantityInStock = 2)
            db.itemDao().insert(item1)
            ItemListFragmentTestUtils.clickOnItem(item1)
            ItemDetailFragmentTestUtils.editItem()
            AddItemFragmentTestUtils.typeInItemFields(item2)
            AddItemFragmentTestUtils.save()
            ItemListFragmentTestUtils.checkIfItemInList(item2)
            ItemListFragmentTestUtils.checkIfItemNotInList(item1)
        }
    }

    @After
    fun tearDown() {
        db.clearAllTables()
    }
}