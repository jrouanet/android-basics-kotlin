package com.inventory.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.inventory.data.Item
import com.inventory.databinding.ItemListItemBinding

/**
 * @param onItemClicked : [com.inventory.ItemListFragment] method to call when an Item of the list is clicked.
 */
class ItemListAdapter(
    val onItemClicked: (Item) -> Unit,
    diffCallback: DiffUtil.ItemCallback<Item>
) :
    ListAdapter<Item, ItemListAdapter.ItemViewHolder>(diffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemViewHolder = ItemViewHolder(
            ItemListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
        return itemViewHolder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
            onItemClicked(getItem(holder.adapterPosition))
        }
    }

    class ItemViewHolder(private val binding: ItemListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Item) {
            binding.item = item
            binding.executePendingBindings()
        }
    }
}