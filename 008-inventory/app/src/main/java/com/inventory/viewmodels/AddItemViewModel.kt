package com.inventory.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.inventory.data.Item
import com.inventory.data.ItemDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Extends [InventoryViewModel] to manage one Item to add/edit in [com.inventory.AddItemFragment]
 */
class AddItemViewModel(itemDao: ItemDao) : InventoryViewModel(itemDao) {

    val item: MutableLiveData<Item> = MutableLiveData()

    fun setItem(itemId: Long): Job? {
        if (itemId == -1L) { //Create mode, initialize one Item.
            item.value = Item(itemName = "", itemPrice = 0.0, quantityInStock = 0)
            return null
        } else { //Edit mode, fetch the item from the database.
            return viewModelScope.launch(Dispatchers.IO) {
                getItem(itemId).collect {
                    item.postValue(it)
                }
            }
        }
    }
}

class AddItemViewModelFactory(private val itemDao: ItemDao) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddItemViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return AddItemViewModel(itemDao) as T
        }
        throw IllegalArgumentException("Unkown ViewModel class")
    }
}