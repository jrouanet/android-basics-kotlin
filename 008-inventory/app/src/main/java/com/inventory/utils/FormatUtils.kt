package com.inventory.utils

import androidx.databinding.InverseMethod
import java.text.NumberFormat

/**
 * Holds utility methods to adapt [com.inventory.data.Item] fields to their layout binding.
 */

fun getFormattedPrice(itemPrice: Double): String {
    return NumberFormat.getCurrencyInstance().format(itemPrice)
}

@InverseMethod("doubleToString")
fun stringToDouble(string: String): Double {
    if (string.isBlank()) return 0.0
    return string.toDouble()
}

fun doubleToString(double: Double): String {
    return double.toString()
}

@InverseMethod("intToString")
fun StringToInt(string: String): Int {
    if (string.isBlank()) return 0
    return string.toInt()
}

fun intToString(int: Int): String {
    return int.toString()
}