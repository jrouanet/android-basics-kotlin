package com.inventory.data

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.recyclerview.widget.DiffUtil
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Defines the database entity and adds the possibility to bind an Item object to a layout.
 */
@Entity(tableName = "item")
data class Item(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    @get:Bindable @ColumnInfo(name = "name") var itemName: String,
    @get:Bindable @ColumnInfo(name = "price") var itemPrice: Double,
    @get:Bindable @ColumnInfo(name = "quantity") var quantityInStock: Int
) : BaseObservable()

/**
 * Used by [com.inventory.adapters.ItemListAdapter].
 */
class ItemDiffUtilCallback : DiffUtil.ItemCallback<Item>() {
    override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem == newItem
    }
}