package com.inventory.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface ItemDao {

    @Insert
    suspend fun insert(item: Item): Long

    @Update
    suspend fun update(item: Item): Unit

    @Delete
    suspend fun delete(vararg item: Item): Unit

    @Query("SELECT * FROM item WHERE id = :id")
    fun getItem(id: Long): Flow<Item> //The "Flow" return type makes the function run in a background thread so we don't need the suspend keyword here.

    @Query("SELECT * from item ORDER BY name ASC")
    fun getItems(): Flow<List<Item>>

}