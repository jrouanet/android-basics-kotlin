package com.inventory.validators

import android.content.Context
import com.inventory.R
import com.inventory.data.Item

/**
 * Holds the data validation logic of [Item] class.
 */
class ItemValidator {

    companion object {

        /**
         * [Item] class fields validation part :
         */
        fun isItemNameValid(itemName: String): Boolean {
            if (itemName.isBlank()) {
                return false
            }
            return true
        }

        fun isItemPriceValid(itemPrice: String): Boolean {
            if (itemPrice.isBlank()) {
                return false
            }
            return true
        }

        fun isItemCountValid(itemCount: String): Boolean {
            if (itemCount.isBlank()) {
                return false
            }
            return true
        }

        fun isEntryValid(itemName: String, itemPrice: String, itemCount: String): Boolean {
            return isItemNameValid(itemName)
                    && isItemPriceValid(itemPrice)
                    && isItemCountValid(itemCount)
        }

        /**
         * Error messages part :
         */
        fun getItemNameErrorMessage(context: Context, itemName: String): String? {
            if (isItemNameValid(itemName)) {
                return null
            } else {
                return context.getString(
                    R.string.error_cannot_be_empty_format,
                    context.getString(R.string.item_name_req)
                )
            }
        }

        fun getItemPriceErrorMessage(context: Context, itemPrice: String): String? {
            if (isItemPriceValid(itemPrice)) {
                return null
            } else {
                return context.getString(
                    R.string.error_cannot_be_empty_format,
                    context.getString(R.string.item_price_req)
                )
            }
        }

        fun getItemCountErrorMessage(context: Context, itemCount: String): String? {
            if (isItemCountValid(itemCount)) {
                return null
            } else {
                return context.getString(
                    R.string.error_cannot_be_empty_format,
                    context.getString(R.string.quantity_req)
                )
            }
        }

        /**
         * Other rules :
         */
        fun isStockAvailable(item: Item?): Boolean {
            if (item == null) return false
            return item.quantityInStock > 0
        }

    }

}