/*
 * Copyright (C) 2021 The Android Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inventory


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.inventory.databinding.FragmentItemDetailBinding
import com.inventory.validators.ItemValidator
import com.inventory.viewmodels.InventoryViewModel
import com.inventory.viewmodels.InventoryViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * [ItemDetailFragment] displays the details of the selected item.
 */
class ItemDetailFragment : Fragment() {
    private val navigationArgs: ItemDetailFragmentArgs by navArgs()
    private var _binding: FragmentItemDetailBinding? = null
    private val binding get() = _binding!!
    private val viewModel: InventoryViewModel by activityViewModels<InventoryViewModel> {
        InventoryViewModelFactory(
            (requireActivity().application as InventoryApplication).database.itemDao()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentItemDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /**
         * As fragment's view lifecycle is shorter that its own lifecycle,
         * we have to reduce the lifespan of the coroutine
         * (because _binding is set to null in onDestroyView).
         */
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            viewModel.getItem(navigationArgs.itemId).collect {
                binding.item = it
            }
        }
        binding.apply {
            deleteItem.setOnClickListener {
                showConfirmationDialog()
            }
            sellItem.setOnClickListener {
                sellItem()
            }
            //Disable the sell button if the item is out of stock :
            sellItem.isEnabled = ItemValidator.isStockAvailable(binding.item)
            editItem.setOnClickListener {
                editItem()
            }
        }
    }

    /**
     * Displays an alert dialog to get the user's confirmation before deleting the item.
     */
    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.delete_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                deleteItem()
            }
            .show()
    }

    /**
     * Deletes the current item and navigates to the list fragment.
     */
    private fun deleteItem() {
        viewModel.deleteItem(binding.item!!)
        findNavController().navigateUp()
    }


    /**
     * Sells the current item (quantity decrements)
     */
    private fun sellItem() {
        binding.item!!.quantityInStock--
        viewModel.updateItem(binding.item!!)
    }

    /**
     * Navigates to [AddItemFragment] with edit mode.
     */
    private fun editItem() {
        findNavController().navigate(
            ItemDetailFragmentDirections.actionItemDetailFragmentToAddItemFragment(
                getString(R.string.edit_fragment_title),
                binding.item!!.id
            )
        )
    }

    /**
     * Called when fragment is destroyed.
     */
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
