/*
 * Copyright (C) 2021 The Android Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inventory

import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.inventory.databinding.FragmentAddItemBinding
import com.inventory.validators.ItemValidator
import com.inventory.viewmodels.AddItemViewModel
import com.inventory.viewmodels.AddItemViewModelFactory

/**
 * Fragment to add or update an item in the Inventory database depending on the value of itemId.
 * -1 is for creation mode, anything else stands for edit mode.
 */
class AddItemFragment : Fragment() {
    private val navigationArgs: ItemDetailFragmentArgs by navArgs()
    private var _binding: FragmentAddItemBinding? = null
    private val binding get() = _binding!!
    private val viewModel: AddItemViewModel by activityViewModels {
        AddItemViewModelFactory(
            (requireActivity().application as InventoryApplication).database.itemDao()
        )
    }

    /**
     * Go back to [ItemListFragment].
     */
    private fun navigateToItemListFragment() {
        Navigation.findNavController(requireView()).navigate(
            AddItemFragmentDirections.actionAddItemFragmentToItemListFragment()
        )
    }

    /**
     * Form validation using [ItemValidator].
     */
    private fun isEntryValid(): Boolean {
        return ItemValidator.isEntryValid(
            binding.itemName.text.toString(),
            binding.itemPrice.text.toString(),
            binding.itemCount.text.toString()
        )
    }

    private fun setErrorMessages() {
        binding.itemName.error = ItemValidator.getItemNameErrorMessage(requireContext(), binding.itemName.text.toString())
        binding.itemPrice.error = ItemValidator.getItemPriceErrorMessage(requireContext(), binding.itemPrice.text.toString())
        binding.itemCount.error = ItemValidator.getItemCountErrorMessage(requireContext(), binding.itemCount.text.toString())
    }

    /**
     * Inserts new Item in database.
     */
    private fun addNewItem() {
        if (isEntryValid()) {
            viewModel.insertItem(viewModel.item.value!!)
            navigateToItemListFragment()
        } else {
            setErrorMessages()
        }
    }

    /**
     * Updates existing Item in database.
     */
    private fun updateItem() {
        if (isEntryValid()) {
            viewModel.updateItem(viewModel.item.value!!)
            navigateToItemListFragment()
        } else {
            setErrorMessages()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddItemBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        viewModel.setItem(navigationArgs.itemId)
        binding.viewModel = viewModel
        return binding.root
    }

    /**
     * Toggles save button behaviour depending on the create/edit mode.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (navigationArgs.itemId == -1L) { //Create mode.
            binding.saveAction.setOnClickListener { addNewItem() }
        } else { //Edit mode.
            binding.saveAction.setOnClickListener { updateItem() }
        }
    }

    /**
     * Called before fragment is destroyed.
     */
    override fun onDestroyView() {
        super.onDestroyView()
        // Hide keyboard.
        val inputMethodManager = requireActivity().getSystemService(INPUT_METHOD_SERVICE) as
                InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(requireActivity().currentFocus?.windowToken, 0)
        _binding = null
    }
}
