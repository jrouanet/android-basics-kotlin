/*
 * Copyright (C) 2021 The Android Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inventory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.inventory.adapters.ItemListAdapter
import com.inventory.data.Item
import com.inventory.data.ItemDiffUtilCallback
import com.inventory.databinding.ItemListFragmentBinding
import com.inventory.viewmodels.InventoryViewModel
import com.inventory.viewmodels.InventoryViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Main fragment displaying details for all items in the database.
 */
class ItemListFragment : Fragment() {

    private var _binding: ItemListFragmentBinding? = null
    private val binding get() = _binding!!
    private val viewModel: InventoryViewModel by activityViewModels<InventoryViewModel> {
        InventoryViewModelFactory(
            (requireActivity().application as InventoryApplication).database.itemDao()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ItemListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * OnItemClicked, navigate to [ItemDetailFragment].
     */
    private fun onItemClicked(item: Item) {
        Navigation.findNavController(requireView()).navigate(
            ItemListFragmentDirections.actionItemListFragmentToItemDetailFragment(item.id)
        )
    }

    /**
     * On fab click, go to [AddItemFragment].
     */
    private fun onAddFabClicked() {
        Navigation.findNavController(requireView()).navigate(
            ItemListFragmentDirections.actionItemListFragmentToAddItemFragment(
                getString(R.string.add_fragment_title)
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ItemListAdapter(::onItemClicked, ItemDiffUtilCallback())
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            viewModel.getItems().collect {
                adapter.submitList(it)
            }
        }
        binding.recyclerView.layoutManager = LinearLayoutManager(this.context)
        binding.recyclerView.adapter = adapter
        binding.floatingActionButton.setOnClickListener {
            onAddFabClicked()
        }
    }
}
