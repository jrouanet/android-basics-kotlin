package com.interactivediceroller.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.google.accompanist.systemuicontroller.rememberSystemUiController


@Composable
fun Theme(content: @Composable () -> Unit) {
    val systemUiController = rememberSystemUiController()
    var systemBarColor: Color
    var colors: Colors

    if (isSystemInDarkTheme()) {
        systemBarColor = darkColors.primary
        colors = darkColors
    } else {
        systemBarColor = lightColors.primary
        colors = lightColors
    }

    systemUiController.setSystemBarsColor(
        color = systemBarColor
    )
    MaterialTheme(
        content = content,
        colors = colors
    )

}