package com.interactivediceroller.ui.main

class Dice(val sides: Int) {

    fun rollDice(): Int {
        return (1..sides).random()
    }
}