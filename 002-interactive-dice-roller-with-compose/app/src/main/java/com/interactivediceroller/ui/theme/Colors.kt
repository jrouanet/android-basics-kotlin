package com.interactivediceroller.ui.theme

import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color


val purple_500 = Color(0xFF6200EE)
val purple_700 = Color(0xFF3700B3)
val teal_200 = Color(0xFF03DAC5)
val teal_700 = Color(0xFF018786)
val black = Color(0xFF000000)
val white = Color(0xFFFFFFFF)
val red_700 = Color(0xffdd0d3c)


val lightColors = lightColors(
    primary = purple_500,
    primaryVariant = purple_700,
    onPrimary = white,
    secondary = teal_200,
    secondaryVariant = teal_700,
    onSecondary = black,
    error = red_700
)

// Just to show dark mode management.
val darkColors = lightColors.copy(background = black)