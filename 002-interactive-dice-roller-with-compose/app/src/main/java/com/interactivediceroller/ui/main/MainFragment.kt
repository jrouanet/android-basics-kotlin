package com.interactivediceroller.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.fragment.app.Fragment
import com.interactivediceroller.R
import com.interactivediceroller.ui.theme.Theme

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                Theme {
                    GetComposition()
                }
            }
        }
    }

    @Composable
    fun GetComposition() {

        val rolledValueState: MutableState<Int> = remember {
            mutableStateOf(1)
        }

        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxHeight()
        ) {
            DiceRollResultImage(rolledValueState.value)
            DiceRollActionButton(rolledValueState)
        }
    }

    @Preview
    @Composable
    fun PreviewGetComposition() {
        GetComposition()
    }
    
    @Composable
    fun DiceRollResultImage(pRolledValue: Int) {
        var painter: Painter = painterResource(
            id = when (pRolledValue) {
                1 -> R.drawable.dice_1
                2 -> R.drawable.dice_2
                3 -> R.drawable.dice_3
                4 -> R.drawable.dice_4
                5 -> R.drawable.dice_5
                6 -> R.drawable.dice_6
                else -> R.drawable.dice_1
            }
        )
        Image(
            painter = painter,
            contentDescription = getString(
                R.string.diceRolledResultContentDescription,
                pRolledValue.toString()
            ),
            modifier = Modifier.width(dimensionResource(id = R.dimen.dice_width))
        )
    }

    @Composable
    fun DiceRollActionButton(pRolledValueState: MutableState<Int>) {
        Button(onClick = {
            Toast.makeText(context, R.string.diceRolled, Toast.LENGTH_SHORT).show()
            pRolledValueState.value = rollDice()
        }) {
            Text(
                text = getString(R.string.diceRollActionButton_Text)
            )
        }
    }

    fun rollDice(): Int {
        val dice: Dice = Dice(6)
        return dice.rollDice()
    }

}