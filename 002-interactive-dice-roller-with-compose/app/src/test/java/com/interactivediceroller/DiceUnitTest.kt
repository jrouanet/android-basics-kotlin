package com.interactivediceroller

import com.interactivediceroller.ui.main.Dice
import org.junit.Assert.assertTrue
import org.junit.Test

class DiceUnitTest {

    @Test
    fun generates_number() {
        val sides: Int = 6
        val dice: Dice = Dice(sides)
        val diceRollResult: Int = dice.rollDice()
        assertTrue("A $sides-dice roll result was not in range [0..$sides], got $diceRollResult.", diceRollResult in 1..sides)
    }

}