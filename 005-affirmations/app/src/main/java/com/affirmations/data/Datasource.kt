package com.affirmations.data

import android.content.Context
import com.affirmations.model.Affirmation

class Datasource(private val context: Context) {

    fun loadAffirmations(): List<Affirmation> {

        val affirmationList: MutableList<Affirmation> = mutableListOf()
        val affirmationCount: Int = 10
        val affirmationTextContentRscName: String = "affirmation"
        val affirmationDrawableRscName: String = "image"
        for (i in 1..affirmationCount) {
            val textContentRscId: Int = context.resources.getIdentifier(
                "%s%d".format(affirmationTextContentRscName, i),
                "string",
                context.packageName
            )
            val drawableContentRscId: Int = context.resources.getIdentifier(
                "%s%d".format(affirmationDrawableRscName, i),
                "drawable",
                context.packageName
            )
            affirmationList.add(
                Affirmation(
                    textContentRscId,
                    drawableContentRscId
                )
            )
        }

        return affirmationList
    }
}