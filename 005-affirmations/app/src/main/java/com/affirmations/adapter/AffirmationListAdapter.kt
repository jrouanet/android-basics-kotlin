package com.affirmations.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.affirmations.model.Affirmation
import com.affirmations.ui.main.AffirmationViewHolder

class AffirmationListAdapter(
    diffUtilCallback: DiffUtil.ItemCallback<Affirmation>
) :
    ListAdapter<Affirmation, AffirmationViewHolder>(diffUtilCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AffirmationViewHolder {
        return AffirmationViewHolder.createInstance(parent)
    }

    override fun onBindViewHolder(holder: AffirmationViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}