package com.affirmations.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.affirmations.model.Affirmation
import com.affirmations.ui.main.AffirmationViewHolder

class AffirmationRecyclerViewAdapter(
    private val affirmationList: List<Affirmation>
) :
    RecyclerView.Adapter<AffirmationViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AffirmationViewHolder {
        return AffirmationViewHolder.createInstance(parent)
    }

    override fun onBindViewHolder(holder: AffirmationViewHolder, position: Int) {
        holder.bind(affirmationList[position])
    }

    override fun getItemCount(): Int {
        return affirmationList.size
    }

}