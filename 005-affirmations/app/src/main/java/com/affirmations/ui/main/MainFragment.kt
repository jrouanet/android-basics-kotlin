package com.affirmations.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.affirmations.R
import com.affirmations.adapter.AffirmationListAdapter
import com.affirmations.data.Datasource
import com.affirmations.databinding.FragmentMainBinding
import com.affirmations.model.Affirmation

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentMainBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        val affirmationList = Datasource(requireContext()).loadAffirmations()
        val adapter = AffirmationListAdapter(Affirmation.AffirmationDiffUtil())

        adapter.submitList(affirmationList)
        binding.recyclerview.adapter = adapter
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }
}