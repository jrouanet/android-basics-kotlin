package com.affirmations.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.affirmations.R
import com.affirmations.databinding.RecyclerviewAffirmationItemBinding
import com.affirmations.model.Affirmation

class AffirmationViewHolder(private val binding: RecyclerviewAffirmationItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun createInstance(pParentView: ViewGroup): AffirmationViewHolder {
            val binding = DataBindingUtil.inflate<RecyclerviewAffirmationItemBinding>(
                LayoutInflater.from(pParentView.context),
                R.layout.recyclerview_affirmation_item,
                pParentView,
                false
            )
            return AffirmationViewHolder(binding)
        }
    }

    fun bind(affirmation: Affirmation) {
        binding.affirmation = affirmation
        binding.executePendingBindings() // Needed by RecyclerViewActions.scrollTo.
    }
}