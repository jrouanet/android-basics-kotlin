package com.affirmations.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.recyclerview.widget.DiffUtil

data class Affirmation(
    @StringRes val textContentRscId: Int,
    @DrawableRes val drawableContentRscId: Int
) {

    class AffirmationDiffUtil : DiffUtil.ItemCallback<Affirmation>() {
        override fun areItemsTheSame(oldItem: Affirmation, newItem: Affirmation): Boolean {
            if (oldItem == newItem) return true
            return false
        }

        override fun areContentsTheSame(oldItem: Affirmation, newItem: Affirmation): Boolean {
            if (oldItem.textContentRscId.equals(newItem.textContentRscId)) return true
            return false
        }
    }

}