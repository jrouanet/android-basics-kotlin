package com.buildabasiclayout.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.fragment.app.Fragment
import com.buildabasiclayout.R

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                Theme {
                    HappyBirthdayPage()
                }
            }
        }
    }


    @Composable
    fun HappyBirthdayPage() {
        Image(
            painter = painterResource(id = R.drawable.androidparty),
            contentDescription = "",
            contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxSize()
        )
        Column(
            modifier = Modifier.fillMaxHeight(),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.Top,
                horizontalArrangement = Arrangement.Start
            ) {
                HappyBirthdayToText("you")
            }
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.Bottom,
                horizontalArrangement = Arrangement.End
            ) {
                HappyBirthdayFromText("Jeremy")
            }
        }
    }

    @Preview
    @Composable
    fun PreviewHappyBirthdayPage() {
        HappyBirthdayPage();
    }

    @Composable
    fun HappyBirthdayToText(pName: String) {
        Text(
            text = stringResource(id = R.string.happy_birthday_to, pName),
            style = MaterialTheme.typography.h5,
            modifier = Modifier.padding(dimensionResource(id = R.dimen.padding))
        )
    }

    @Preview
    @Composable
    fun PreviewHappyBirthdayToText() {
        HappyBirthdayToText(pName = "You")
    }

    @Composable
    fun HappyBirthdayFromText(pName: String) {
        Text(
            text = stringResource(id = R.string.happy_birthday_from, pName),
            style = MaterialTheme.typography.h5,
            modifier = Modifier.padding(dimensionResource(id = R.dimen.padding))
        )
    }

    @Preview
    @Composable
    fun PreviewHappyBirthdayFromText() {
        HappyBirthdayFromText("Jérémy")
    }

}