package com.buildabasiclayout.ui.main

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import com.buildabasiclayout.ui.theme.lightColors



@Composable
fun Theme(content: @Composable () -> Unit) {
    MaterialTheme(
        content = content,
        colors = lightColors
    )
}
