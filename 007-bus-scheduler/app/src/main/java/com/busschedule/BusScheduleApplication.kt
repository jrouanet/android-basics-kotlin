package com.busschedule

import android.app.Application
import com.busschedule.db.AppDatabase

class BusScheduleApplication : Application() {

    val database: AppDatabase by lazy {
        AppDatabase.getDatabase(this)
    }

}