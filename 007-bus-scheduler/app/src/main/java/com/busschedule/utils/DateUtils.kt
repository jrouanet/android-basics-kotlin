package com.busschedule.utils

import java.text.SimpleDateFormat
import java.util.*


fun intToStringFormat(timestampAsInt: Int): String {
    return SimpleDateFormat("h:mm a")
        .format(Date(timestampAsInt.toLong() * 1000))
}