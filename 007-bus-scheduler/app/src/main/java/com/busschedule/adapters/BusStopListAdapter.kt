package com.busschedule.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.busschedule.R
import com.busschedule.databinding.BusStopItemBinding
import com.busschedule.db.schedule.Schedule
import com.busschedule.db.schedule.ScheduleDiffCallback

class BusStopListAdapter(
    private val onItemClicked: (Schedule) -> Unit,
    diffCallback: ScheduleDiffCallback
) : ListAdapter<Schedule, BusStopListAdapter.BusStopViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusStopViewHolder {
        val binding = BusStopItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        val busStopViewHolder = BusStopViewHolder(binding)
        busStopViewHolder.itemView.setOnClickListener {
            onItemClicked(getItem(busStopViewHolder.adapterPosition))
        }
        return busStopViewHolder
    }

    override fun onBindViewHolder(holder: BusStopViewHolder, position: Int) {
        val schedule: Schedule = getItem(position)
        holder.bind(schedule)
    }

    class BusStopViewHolder(private val binding: BusStopItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(schedule: Schedule) {
            binding.schedule = schedule
            binding.executePendingBindings()
        }
    }
}