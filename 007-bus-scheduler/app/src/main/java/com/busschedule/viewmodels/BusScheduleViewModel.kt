package com.busschedule.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.busschedule.db.schedule.Schedule
import com.busschedule.db.schedule.ScheduleDao
import kotlinx.coroutines.flow.Flow

class BusScheduleViewModel(application: Application, val scheduleDao: ScheduleDao) :
    AndroidViewModel(application) {

    fun fullSchedule(): Flow<List<Schedule>> = scheduleDao.getAll()
    fun scheduleForStopName(stopName: String): Flow<List<Schedule>> = scheduleDao.getByStopName(stopName)
}


class BusScheduleViewModelFactory(val application: Application, val scheduleDao: ScheduleDao) :
    ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(BusScheduleViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return BusScheduleViewModel(application, scheduleDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}