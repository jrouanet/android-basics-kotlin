package com.tipcalculator.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel() : ViewModel() {

    data class Service(
        var cost: Double = 0.0,
        var rate: Double = Quality.RATES.get(Quality.AMAZING)!!,
        var roundUpTip: Boolean = false
    ) {

        var tip = MutableLiveData<Double>(calculateTip())

        fun calculateTip(): Double {
            var tip: Double = cost * rate
            if (roundUpTip) {
                tip = Math.ceil(tip)
            }
            return tip
        }

        fun updateTip(): Unit {
            tip.value = calculateTip()
        }
    }

    var service = Service()
}