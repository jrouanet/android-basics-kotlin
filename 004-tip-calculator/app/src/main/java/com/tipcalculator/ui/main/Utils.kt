package com.tipcalculator.ui.main

import android.icu.text.NumberFormat
import androidx.databinding.InverseMethod


@InverseMethod("doubleToString")
fun stringToDouble(str: String): Double {
    val double = str.toDoubleOrNull() ?: return 0.0
    return double
}

fun doubleToString(double: Double): String {
    return double.toString()
}

fun formatCurrency(amount: Double): String {
    return NumberFormat.getCurrencyInstance().format(amount)
}
