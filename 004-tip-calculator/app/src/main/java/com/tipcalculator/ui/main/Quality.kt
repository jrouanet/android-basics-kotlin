package com.tipcalculator.ui.main

class Quality {
    companion object {
        val AMAZING = "amazing"
        val GOOD = "good"
        val OKAY = "okay"

        var RATES: LinkedHashMap<String, Double> = linkedMapOf(
            AMAZING to 0.20,
            GOOD to 0.18,
            OKAY to 0.15,
        )
    }
}