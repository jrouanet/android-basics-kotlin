package com.tipcalculator.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.radiobutton.MaterialRadioButton
import com.tipcalculator.R
import com.tipcalculator.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentMainBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        binding.viewModel = viewModel
        binding.setLifecycleOwner { lifecycle }

        setUpRadioGroup(binding)

        return binding.root
    }

    /**
     * Sets the content of the RadioGroup dynamically.
     */
    private fun setUpRadioGroup(binding: FragmentMainBinding) {
        /**
         * Key holds the index of the button in the RadioGroup.
         * Value holds the corresponding resource.
         */
        var radioGroupIndexMapping = HashMap<Int, Map.Entry<String, Double>>()
        var index: Int = 1
        Quality.RATES.forEach {
            radioGroupIndexMapping.put(index, it)
            index++
        }

        /**
         * Add RadioButtons to the RadioGroup :
         */
        radioGroupIndexMapping.forEach {
            var index: Int = it.key - 1 //Weh inserting, index starts at zero.
            var qualityLevel: String = it.value.key
            var rate: Double = it.value.value
            var materialButton = MaterialRadioButton(requireContext())

            materialButton.setText(
                requireContext().getString(
                    R.string.radiobutton_format,
                    qualityLevel,
                    (rate * 100).toInt()
                )
            )
            binding.radiogroupRate.addView(
                materialButton,
                index
            )
        }

        binding.radiogroupRate.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener() { radioGroup, i ->
            viewModel.service.rate = radioGroupIndexMapping.get(i)!!.value
        })

        (binding.radiogroupRate.getChildAt(0) as MaterialRadioButton).isChecked = true
    }

}