package com.marsphotos

import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.marsphotos.network.MarsPhoto
import com.marsphotos.overview.MarsApiStatus
import com.marsphotos.overview.PhotoGridListAdapter

/**
 * This file holds the Binding Adapters used in the layouts.
 */

/**
 * Used by grid_view_item.xml
 */
@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        imgView.load(imgUri) {
            placeholder(R.drawable.loading_animation)
            error(R.drawable.ic_broken_image)
        }
    }
}

/**
 * Used by fragment_overview.xml to update list content.
 */
@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<MarsPhoto>?) {
    val adapter = recyclerView.adapter as PhotoGridListAdapter
    adapter.submitList(data)
}

/**
 * used by fragment_overview.xml to handle loading and network errors.
 */
@BindingAdapter("marsApiStatus")
fun bindStatus(statusImageView: ImageView, marsApiStatus: MarsApiStatus) {
    when (marsApiStatus) {
        MarsApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        MarsApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        MarsApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}