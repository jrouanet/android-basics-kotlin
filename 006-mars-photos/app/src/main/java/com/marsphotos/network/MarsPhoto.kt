package com.marsphotos.network

import androidx.recyclerview.widget.DiffUtil
import com.squareup.moshi.Json

/**
 * Class used to present data.
 */
data class MarsPhoto(
    @Json(name = "id") val id: String,
    @Json(name = "img_src") val imgSrcUrl: String
) {

    /**
     * DiffUtilCallback used by [com.marsphotos.overview.PhotoGridListAdapter]
     */
    companion object DiffUtilCallback : DiffUtil.ItemCallback<MarsPhoto>() {
        override fun areItemsTheSame(oldItem: MarsPhoto, newItem: MarsPhoto): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MarsPhoto, newItem: MarsPhoto): Boolean {
            return oldItem.imgSrcUrl == newItem.imgSrcUrl
        }
    }
}