package com.marsphotos.overview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.marsphotos.databinding.GridViewItemBinding
import com.marsphotos.network.MarsPhoto

class PhotoGridListAdapter(diffCallback: DiffUtil.ItemCallback<MarsPhoto>) :
    ListAdapter<MarsPhoto, PhotoGridListAdapter.MarsPhotoViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarsPhotoViewHolder {
        val binding = GridViewItemBinding.inflate(LayoutInflater.from(parent.context))
        return MarsPhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MarsPhotoViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class MarsPhotoViewHolder(private val binding: GridViewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(marsPhoto: MarsPhoto) {
            binding.marsPhoto = marsPhoto
            binding.executePendingBindings()
        }
    }
}